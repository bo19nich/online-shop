import React from "react";
import Signup from "../../components/sign-up/Signup.component";
import Signin from "../../components/signin/Signin.component";
import "./authenticationPage.styles.scss";

function AuthenticationPage() {
  return (
    <div className="sign-in-and-sign-up">
      <Signin />
      <Signup />
    </div>
  );
}

export default AuthenticationPage;
