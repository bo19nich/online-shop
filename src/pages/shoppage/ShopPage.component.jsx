import React from "react";
import CollectionOverviewComponent from "../../components/collection-overview/CollectionOverview.component";

const ShopPage = () => (
  <div className="shop-page">
    <CollectionOverviewComponent />
  </div>
);

export default ShopPage;
