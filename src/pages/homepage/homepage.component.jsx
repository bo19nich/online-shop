import React from "react";
import Directory from "../../components/directory";
import "./homepage.styles.scss";

function HomePage() {
  return (
    <div className="homepage">
      <Directory />
    </div>
  );
}

export default HomePage;
