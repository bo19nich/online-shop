import { createSelector } from "reselect"

const selectShopItems = state => state.shopItems

export const selectCollections = createSelector(
    [selectShopItems],
    shopItems => shopItems.collections
)