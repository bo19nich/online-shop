import React from "react";
import { withRouter } from "react-router-dom";
import bgImg from "../../imgs/1.jpg";
import "./menu-item.styles.scss";

function MenuItem({ title, imageUrl, size, history, match, linkUrl }) {
  return (
    <div
      className={`${size} menu-item`}
      onClick={() => history.push(`${match.url}${linkUrl}`)}
    >
      <div
        style={{
          backgroundImage: `url(${imageUrl})` || { bgImg },
        }}
        className="background-image"
      />
      <div className="content">
        <h1 className="title">{title}</h1>
        <span className="subtitle">SHOP NOW</span>
      </div>
    </div>
  );
}

export default withRouter(MenuItem);
