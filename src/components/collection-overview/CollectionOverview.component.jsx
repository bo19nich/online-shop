import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { selectCollections } from "../../redux/shop/shop.selectors";
import CollectionPreview from "../collection-preview/CollectionPreview.component";
import "./collection-overview.styles.scss";

function CollectionOverview({ collection }) {
  return (
    <div className="collection-overview">
      {collection.map(({ id, ...otherCollectionProps }) => (
        <CollectionPreview key={id} {...otherCollectionProps} />
      ))}
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  collection: selectCollections,
});

export default connect(mapStateToProps)(CollectionOverview);
