import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'


const config = {
    apiKey: "AIzaSyCOF8YCO74Six0F-v7GdgBHqDO3piFEg6Y",
    authDomain: "tubayo.firebaseapp.com",
    databaseURL: "https://tubayo.firebaseio.com",
    projectId: "tubayo",
    storageBucket: "tubayo.appspot.com",
    messagingSenderId: "557669005253",
    appId: "1:557669005253:web:cd16e7692158f323b73dac",
    measurementId: "G-6848MNKXZN"
}

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return

    const userRef = firestore.doc(`users/${userAuth.uid}`)
    const snapShot = await userRef.get()
    // console.log('snapShot', snapShot)
    if (!snapShot.exists) {
        const { displayName, email } = userAuth
        const createdAt = new Date()

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch (error) {
            console.log('Error creating user: ', error)
        }
    }

    return userRef

}

firebase.initializeApp(config)

export const auth = firebase.auth()
export const firestore = firebase.firestore()

const provider = new firebase.auth.GoogleAuthProvider()
provider.setCustomParameters({ 'promt': 'select_account' })
export const signInWithGoogle = () => auth.signInWithPopup(provider)

export default firebase